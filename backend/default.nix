{ pkgs ?  import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") { } }:

let

  _backend = pkgs.haskellPackages.callCabal2nix "backend" ./. {
    common = pkgs.haskellPackages.callCabal2nix "common" ../common {};
  };

  _backend_full = pkgs.stdenv.mkDerivation {
    name = "backend_full";
    src = ./.;
    buildInput = [ _backend ];

    installPhase = ''
      mkdir -p $out/bin
      cp ${_backend}/bin/* $out/bin
      cp -R ./static $out/bin/
    '';
  };

in

if pkgs.lib.inNixShell then _backend.env else _backend_full

