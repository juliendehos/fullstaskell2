{-# LANGUAGE OverloadedStrings #-}

import qualified Clay as C
import Common
import Data.Maybe (fromMaybe)
import Data.Text.Lazy (toStrict, unpack)
import Lucid
import Network.HTTP.Types.Status (badRequest400)
import Network.Wai.Middleware.Cors (simpleCors)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (static)
import Web.Scotty (get, html, json, middleware, param, scotty, status, text)
import System.Environment (lookupEnv)
import Text.Read (readMaybe)

main :: IO ()
main = do
    portMayStr <- lookupEnv "PORT"
    let port = fromMaybe 3000 (portMayStr >>= readMaybe)

    scotty port $ do

        middleware logStdoutDev

        middleware simpleCors

        get "/" $ html $ renderText $ html_ $ do
            head_ $ style_ $ toStrict $ C.render $ do
                C.body C.? C.backgroundImage  (C.url "static/haskell.jpg")
            body_ $ do
                h1_ "Fullstaskell2"
                ul_ $ do
                    li_ $ a_ [href_ "api/square/42"] "api/square/42"
                    li_ $ a_ [href_ "frontend_js/index.html"] "frontend_js"
                    li_ $ a_ [href_ "frontend_miso/index.html"] "frontend_miso"
                    li_ $ a_ [href_ "frontend_reflex/index.html"] "frontend_reflex"
                    li_ $ a_ [href_ "frontend_elm/index.html"] "frontend_elm"
                    li_ $ a_ [href_ "frontend_purescript/index.html"] "frontend_purescript"

        get "/api/square/:x" $ do
            xMaybe <- readMaybe <$> unpack <$> param "x" 
            case xMaybe of Nothing -> text "not a number" >> status badRequest400
                           Just x  -> json $ computeSquare x

        middleware static

