{ pkgs ?  import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/17.09.tar.gz") { } }:

let

  _common = pkgs.haskell.packages.ghcjs.callCabal2nix "common" ../common {};

  _miso = pkgs.haskell.packages.ghcjs.callCabal2nix "miso" (pkgs.fetchFromGitHub {
    rev = "0.21.1.0";
    sha256 = "19x9ym4399i6ygs0hs9clgrvni0vijfg4ff3jfxgfqgjihbn0w4r";
    owner = "haskell-miso";
    repo = "miso";
  }) {};

  frontend_miso = pkgs.haskell.packages.ghcjs.callCabal2nix "frontend_miso" ./. {
    common = _common;
    miso = _miso; 
  };

  inherit (pkgs) closurecompiler;

  frontend_miso_pkg = pkgs.runCommand "frontend_miso_pkg" { inherit frontend_miso; } ''
    mkdir -p $out
    cp -R ${frontend_miso.src}/static $out/
    cp ${frontend_miso.src}/index.html $out/
    cd ${frontend_miso}/bin/frontend-miso.jsexe
    ${closurecompiler}/bin/closure-compiler all.js > $out/all.min.js
  '';

in 

if pkgs.lib.inNixShell then frontend_miso.env else frontend_miso_pkg

