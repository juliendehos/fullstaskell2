let

  config = {
    packageOverrides = pkgs: rec {
      haskellPackages = pkgs.haskellPackages.override {
        overrides = haskellPackagesNew: haskellPackagesOld: rec {
          common = pkgs.haskellPackages.callCabal2nix "common" ../common {};
        };
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };

  drv = pkgs.haskellPackages.callCabal2nix "backend" ./. {};

in

if pkgs.lib.inNixShell then drv.env else drv

