{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  name = "frontend_js";
  src = ./.;
  installPhase = ''
    mkdir -p $out
    cp -R static $out/
    cp index.html $out/
  '';
}

