# frontend_elm


## build

```
nix-build
firefox result/index.html
```


## shell

- with elm-make:

```
nix-shell
elm-make frontend_elm.elm --output all.min.js
firefox index.html
```

- with elm-reactor:

```
nix-shell
elm-reactor
```

<http://localhost:8000>

