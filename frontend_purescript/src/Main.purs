module Main where

import Control.Monad.Aff (Aff)
import Control.Monad.Eff (Eff)
import Data.Argonaut (class DecodeJson, decodeJson, (.?))
import Data.Either (hush)
import Data.Maybe (Maybe(..))
import Halogen as H
import Halogen.Aff as HA
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.VDom.Driver (runUI)
import Network.HTTP.Affjax as AX
import Prelude

main :: Eff (HA.HalogenEffects (ajax :: AX.AJAX)) Unit
main = HA.runHalogenAff do
  body <- HA.awaitBody
  runUI ui unit body

ui :: forall eff. H.Component HH.HTML Query Unit Void (Aff (ajax :: AX.AJAX | eff))
ui = H.component
    { initialState: const initialState
    , render
    , eval
    , receiver: const Nothing
    }

newtype Square = Square { x :: Number, x2 :: Number } 

instance decodeJsonTodo :: DecodeJson Square where
    decodeJson json = do
        obj <- decodeJson json
        x <- obj .? "squareX"
        x2 <- obj .? "squareX2"
        pure $ Square { x, x2 }

type State = { square :: Maybe Square }

data Query a = ReqApiSquare String a

initialState :: State
initialState = { square: Nothing }

render :: State -> H.ComponentHTML Query
render st =
    HH.div []
        [ HH.p []
            [ HH.a 
                [ HP.href "../" ] 
                [ HH.text "home page" ]
            ]
        , HH.h1 [] [ HH.text "Compute Square (frontend_purescript)" ]
        , HH.input [ HE.onValueInput (HE.input ReqApiSquare) ]
        , HH.p []
            [ HH.text $ case st.square of
                  Nothing -> ""
                  Just (Square sq) -> show sq.x <> "^2 = " <> show sq.x2
            ]
        ]

eval :: forall eff. Query ~> H.ComponentDSL State Query Void (Aff (ajax :: AX.AJAX | eff))
eval (ReqApiSquare username next) = do
        H.modify (_ { square = Nothing })
        response <- H.liftAff $ AX.get ("/api/square/" <> username)
        let sq = hush $ decodeJson response.response 
        H.modify (_ { square = sq })
        pure next

