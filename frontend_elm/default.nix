{ pkgs ?  import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") { } }:

let

  frontend_elm_pkg = pkgs.stdenv.mkDerivation {
    name = "frontend_elm";
    src = ./.;
    buildInputs = [ pkgs.elmPackages.elm-make ];
    buildPhase = ''
      export HOME=`pwd`
      elm-make frontend_elm.elm --yes --output all.js
      ${pkgs.closurecompiler}/bin/closure-compiler all.js --compilation_level=ADVANCED_OPTIMIZATIONS --jscomp_off=checkVars > all.min.js
    '';
    installPhase = ''
      mkdir -p $out
      cp -R static $out/
      cp index.html all.min.js $out/
    '';
  };

  frontend_elm_env = with pkgs; stdenv.mkDerivation {
    name = "frontend_elm_env";
    src = ./.;
    buildInputs = with elmPackages; [
      elm-format
      elm-make
      elm-reactor
      elm-repl
    ];
    installPhase = "";
  };

in

if pkgs.lib.inNixShell then frontend_elm_env else frontend_elm_pkg

