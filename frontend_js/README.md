# frontend_js

## initial setup (for running locally)

- run the backend server

- change the base url of the API to `http://localhost:3000` in `index.html`


## build

```
nix-build
firefox result/index.html
```

## shell

```
firefox index.html
```

