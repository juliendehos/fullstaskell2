# frontend_reflex

## initial setup (for running locally)

- run the backend server

- change the base url of the API to `http://localhost:3000` in `frontend_reflex/frontend_reflex.hs`


## build

```
$ nix-build
$ firefox result/index.html
```

## shell

```
$ nix-shell
$ cabal new-build all
$ firefox dist-newstyle/build/x86_64-linux/ghcjs-0.2.1/frontend-reflex-0.1.0.0/c/frontend-reflex/build/frontend-reflex/frontend-reflex.jsexe/index.html 
```

