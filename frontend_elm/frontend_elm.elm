module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (href)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Decode as Decode


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Square =
    { x : Float, x2 : Float }


type alias Model =
    { maybeSquare : Maybe Square }


init : ( Model, Cmd Msg )
init =
    ( Model Nothing, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


type Msg
    = NewInput String
    | NewApiSquare (Result Http.Error Square)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NewInput txt ->
            ( model, getSquare txt )

        NewApiSquare (Ok s) ->
            ( Model (Just s), Cmd.none )

        NewApiSquare (Err _) ->
            ( Model Nothing, Cmd.none )


getSquare : String -> Cmd Msg
getSquare txt =
    let
        url =
            -- "http://localhost:3000/api/square/" ++ txt
            "/api/square/" ++ txt
    in
    Http.send NewApiSquare (Http.get url decodeApiSquare)


decodeApiSquare : Decode.Decoder Square
decodeApiSquare =
    Decode.map2 Square
        (Decode.field "squareX" Decode.float)
        (Decode.field "squareX2" Decode.float)


view model =
    div []
        [ p [] [ a [ href "../" ] [ text "home page" ] ]
        , h1 [] [ text "Compute Square (frontend_elm)" ]
        , input [ onInput NewInput ] []
        , p []
            [ text
                (case model.maybeSquare of
                    Nothing ->
                        ""

                    Just s ->
                        toString s.x ++ "^2 = " ++ toString s.x2
                )
            ]
        ]
