# frontend_miso

## initial setup (for running locally)

- run the backend server

- change the base url of the API to `http://localhost:3000` in `frontend_miso.hs`

## build

```
$ nix-build
$ firefox result/index.html
```

## shell

```
$ firefox "http://localhost:6400/"
$ nix-shell
$ cabal new-repl
> import Miso.Dev  # Once
> :r               # For recompiling the sources
> clearBody >> main
```

