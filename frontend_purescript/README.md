# frontend_purescript

## initial setup (for running locally)

- run the backend server

- change the base url of the API to `http://localhost:3000` in `src/Main.purs`

- **may not work, because of CORS**


## build

```
nix-build
firefox result/index.html
```


## shell

```
nix-shell

rm -rf bower_components node_modules .pulp-cache

bower install

npm run build0
firefox index.html
```

