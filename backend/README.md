# backend

Warning: use backend locally for testing API or home page only (frontends won't work).


## build

```
nix-build
./result/bin/backend
```

<http://localhost:3000/api/square/42>

<http://localhost:3000>


## shell

```
nix-shell
cabal run
```

<http://localhost:3000/api/square/42>

<http://localhost:3000>

