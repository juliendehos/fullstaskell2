{-# LANGUAGE OverloadedStrings #-}

import Common (Square(..))
import Data.Default (def)
import Data.Maybe (maybe)
import Data.Monoid ((<>))
import Data.Text (pack, Text)
import Reflex (holdDyn)
import Reflex.Dom 
import Reflex.Dom.Xhr (decodeXhrResponse, performRequestAsync, XhrRequest(..))

main :: IO ()
main = mainWidget $ do 
    el "p" $ elAttr "a" ("href" =: "../") $ text "home page"
    el "h1" $ text "Compute Square (frontend_reflex)"
    myInput <- textInput def
    evStart <- getPostBuild
    let evs = [ () <$ _textInput_input myInput , evStart ]
        evCode = tagPromptlyDyn (value myInput) (leftmost evs)
    evResponse <- performRequestAsync $ reqSquare <$> evCode
    let evResult = ((maybe "" fmtSquare) . decodeXhrResponse) <$> evResponse
    el "p" $ dynText =<< holdDyn "" evResult 

reqSquare :: Text -> XhrRequest ()
reqSquare code = XhrRequest "GET" ("/api/square/" <> code) def
-- reqSquare code = XhrRequest "GET" ("http://localhost:3000/api/square/" <> code) def

fmtSquare :: Square -> Text
fmtSquare (Square x x2 _) = pack $ show x ++ "^2 = " ++ show x2

