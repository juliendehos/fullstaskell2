{ pkgs ? import <nixpkgs> {} }:

let

  _backend = import ./backend/default.nix {};
  _frontend_js = import ./frontend_js/default.nix {};
  _frontend_miso = import ./frontend_miso/default.nix {};
  _frontend_reflex = import ./frontend_reflex/default.nix {};
  _frontend_elm = import ./frontend_elm/default.nix {};
  _frontend_purescript = import ./frontend_purescript/default.nix {};

in

pkgs.stdenv.mkDerivation {
  name = "fullstaskell2";
  src = ./.;

  buildInput = [
    _backend
    _frontend_js
    _frontend_miso
    _frontend_reflex
    _frontend_elm
    _frontend_purescript
  ];

  installPhase = ''
    mkdir -p $out/bin
    cp -R ${_backend}/bin/* $out/bin/

    mkdir -p $out/bin/frontend_js
    cp -R ${_frontend_js}/* $out/bin/frontend_js/

    mkdir -p $out/bin/frontend_miso
    cp -R ${_frontend_miso}/* $out/bin/frontend_miso/

    mkdir -p $out/bin/frontend_reflex
    cp -R ${_frontend_reflex}/* $out/bin/frontend_reflex/

    mkdir -p $out/bin/frontend_elm
    cp -R ${_frontend_elm}/* $out/bin/frontend_elm/

    mkdir -p $out/bin/frontend_purescript
    cp -R ${_frontend_purescript}/* $out/bin/frontend_purescript/
  '';
}

