{ compiler ? "ghc822" }:

let
  config = {
    packageOverrides = pkgs: rec {
      haskell = pkgs.haskell // {
        packages = pkgs.haskell.packages // {
          "${compiler}" = pkgs.haskell.packages."${compiler}".override {
            overrides = haskellPackagesNew: haskellPackagesOld: rec {
              common = haskellPackagesNew.callCabal2nix "common" ../common {};
              backend = haskellPackagesNew.callCabal2nix "backend" ./. {};
            };
          };
        };
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };

  drv = pkgs.haskell.packages.${compiler}.backend;

in

if pkgs.lib.inNixShell then drv.env else drv

