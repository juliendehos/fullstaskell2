# { pkgs ?  import <nixpkgs> { } }:
# { pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") { } }:
{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/10b979ff213d7e63a6e072d0433a21687e311483.tar.gz") {} }:

let

  frontend_purescript_pkg = pkgs.stdenv.mkDerivation {
    name = "frontend_purescript";
    src = ./.;
    buildInputs = with pkgs; [
      closurecompiler
      cacert
      git
      nodejs-8_x
      nodePackages.bower
      nodePackages.pulp
      purescript
    ];
    buildPhase = ''
      export HOME=`pwd`
      bower install
      pulp build --to all.js
      ${pkgs.closurecompiler}/bin/closure-compiler all.js > all.min.js
    '';
    installPhase = ''
      mkdir -p $out
      cp -R static $out/
      cp index.html all.min.js $out/
    '';
  };

  frontend_purescript_env = pkgs.stdenv.mkDerivation {
    name = "frontend_purescript_env";
    src = ./.;
    buildInputs = with pkgs; [
      git
      nodejs-8_x
      nodePackages.bower
      nodePackages.pulp
      purescript
    ];
    installPhase = "";
  };

in

if pkgs.lib.inNixShell then frontend_purescript_env else frontend_purescript_pkg


