{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import           Common (Square (..))
import           Data.Aeson (decodeStrict)
import qualified JavaScript.Web.XMLHttpRequest as XHR
import           Miso
import qualified Miso.String as MS

data Model 
    = Model 
        { modelSquare :: Maybe Square 
        } deriving (Eq, Show)

data Action 
    = FetchSquare MS.MisoString 
    | SetSquare (Maybe Square) 
    | NoOp 
    deriving (Show, Eq)

main :: IO ()
main = startApp App 
    { model = Model Nothing
    , update = updateModel
    , view = viewModel
    , subs = []
    , events = defaultEvents
    , initialAction = NoOp
    , mountPoint = Nothing
    }

updateModel :: Action -> Model -> Effect Action Model
updateModel (FetchSquare str) m = m <# (SetSquare <$> queryApi str)
updateModel (SetSquare ms) m = noEff m { modelSquare = ms }
updateModel NoOp m = noEff m

viewModel :: Model -> View Action
viewModel (Model info) = div_ []
    [ a_ [ href_ "../" ] [ text "home page" ]
    , h1_ [] [ text "Compute Square (frontend_miso)" ]
    , input_ [ onInput FetchSquare ] 
    , p_ [] $ case info of 
        Nothing -> [ ]
        Just (Square x x2 _) -> [ text (MS.toMisoString x) , text "^2 = " , text (MS.toMisoString x2) ]
    ]

queryApi :: MS.MisoString -> IO (Maybe Square)
queryApi str = do
    -- let uri = MS.pack $ "http://localhost:3000/api/square/" ++ MS.fromMisoString str
    let uri = MS.pack $ "/api/square/" ++ MS.fromMisoString str
        req = XHR.Request XHR.GET uri Nothing [] False XHR.NoData
    Just contents <- XHR.contents <$> XHR.xhrByteString req
    return $ decodeStrict contents

        {-
        req = XHR.Request { XHR.reqMethod = XHR.GET
                          , XHR.reqURI = uri
                          , XHR.reqLogin = Nothing
                          , XHR.reqHeaders = []
                          , XHR.reqWithCredentials = False
                          , XHR.reqData = XHR.NoData
                          }
        -}

