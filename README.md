
# fullstaskell2

Fullstack web development with hakell (+ nix, elm and purescript).

![](doc/screen-2018-08-13.mp4)

Full setup/build takes about half an hour (using binary caches)…

## setup

- install [Nix or NixOS](https://nixos.org/)

- add binary caches (`/etc/nixos/configuration.nix` or `/etc/nix/nix.conf`) :

```
  nix.binaryCaches = [ 
    "https://cache.nixos.org/"
    "https://nixcache.reflex-frp.org"  # haskell reflex
    "https://hydra.dmj.io"             # haskell miso
  ];
  nix.binaryCachePublicKeys = [ 
    "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI="
  ];
```


## build

```
nix-build
cd result/bin
./backend
```

<http://localhost:3000>


## shell

see individual directories : backend, common, frontend...

