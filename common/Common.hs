{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Common where

import Data.Aeson (FromJSON, ToJSON)
import Data.Text (Text)
import GHC.Generics (Generic)

data Square = Square
    { squareX :: Double
    , squareX2 :: Double
    , squareInfo :: Text
    } deriving (Eq, Generic, Show)


instance FromJSON Square 

instance ToJSON Square

computeSquare :: Double -> Square
computeSquare x = Square { squareX = x, squareX2 = x*x, squareInfo = "from computeSquare" }

